// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_0_1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_0_1, "TDS_0_1" );

DEFINE_LOG_CATEGORY(LogTDS_0_1)
 