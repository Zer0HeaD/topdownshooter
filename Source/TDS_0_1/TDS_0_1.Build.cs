// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_0_1 : ModuleRules
{
	public TDS_0_1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
