// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_0_1GameMode.h"
#include "TDS_0_1PlayerController.h"
//#include "Character/TDS_0_1Character.h"
#include "TDS_0_1Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS_0_1GameMode::ATDS_0_1GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_0_1PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/BluePrints/MainCharacter/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
